<?php
require_once('filebrowser.php');

$currentPath = (isset($_GET["currentPath"]) ? $_GET["currentPath"] : null);
$strExtensions = (isset($_GET["extensions"]) ? $_GET["extensions"] : "");
$arrayExtensions = explode(",", $strExtensions);

$fileBrowser = new FileBrowser("/home/eduardo", $currentPath, $arrayExtensions);
$files = $fileBrowser->Get();

?>
<!doctype html>
<html lang="en">
 <head>
  <title>File browser</title>
  <link rel="stylesheet" type="text/css" href="style.css">
 </head>
 <body>
     <div class="header">
         <span class="left">File Browser</span>
         <span class="right">
             Filter by extensions, separate them using commas: 
             <form>
                 <input type="hidden" name="currentPath" value="<?=$currentPath;?>"/>
                 <input type="text" name="extensions" value="<?=$strExtensions;?>"/>
             </form>
         </span>
     </div>
     <div class="body">
        <?php foreach($files as $file): ?>
            <div class="each">
                <?php if($file["directory"] === true): ?>
                   <span class="directory">
                       <a href="index.php?currentPath=<?=$file["path"]?>&extensions=<?=$strExtensions;?>">
                <?php else: ?>
                   <span class="file">
                       <a href="file://<?=$file["path"];?>" target="_blank">
                <?php endif; ?>
                <?= $file["name"];?></a></span>
            </div>
        <?php endforeach; ?>
    </div>
 </body>
</html>