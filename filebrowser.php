<?php
    require_once('interface.php');

class FileBrowser implements __FileBrowser {
    
    private $rootPath;
    private $currentPath;
    private $extensionFilter;
    
    public function __construct($rootPath, $currentPath = null, array $extensionFilter = array()) {
        
        $this->SetRootPath($rootPath);
        
        // In case current path is null, the root becomes the currentPath
        if ($currentPath === null){
            $currentPath = $rootPath;
        }
        $this->SetCurrentPath($currentPath);
        
        $this->SetExtensionFilter($extensionFilter);
        
    }
    
    public function Get() {
        
        $files = scandir($this->currentPath);
        $filesArray = [];
        
        // Adding option to go back one level
        if ($this->CheckIfCanGoUp()){
            array_push($filesArray, ["path"      =>  urlencode(dirname($this->currentPath)), 
                                     "directory" =>  true,
                                     "name"      =>  "<-Up"]);
            
        }
        
        // Looping through file to apply necessary checks
        foreach($files as $file) {
            
            // Removing standard go ups
            if ($file == "." || $file == ".."){
                continue;
            }
            
            $path = $this->currentPath . "/" . $file;
            $directory = $this->CheckIfDirectory($path);
            
            // Extension filter
            if ((count($this->extensionFilter) < 1) or (!$this->IsExtensionInList($path))) {
                continue;
            }
            
            array_push($filesArray, ["path"      =>  $path, 
                                     "directory" =>  $directory,
                                     "name"      =>  $file]);
            
        }
        
        return $filesArray;
        
    }

    public function SetCurrentPath($currentPath) {
        
        $this->currentPath = $currentPath;
        
    }

    public function SetExtensionFilter(array $extensionFilter) {
        
        $this->extensionFilter = $extensionFilter;
        
    }

    public function SetRootPath($rootPath) {
        
        $this->rootPath = $rootPath;
        
    }
    
    public function CheckIfCanGoUp() {
        
        if (urlencode($this->currentPath) == urlencode($this->rootPath)) {
            return false;
        } else {
            return true;
        }
        
    }
    
    public function CheckIfDirectory($path) {
        
        if (is_dir($path)) {
            return true;
        } else {
            return false;
        }
        
    }
    
    
    public function IsExtensionInList($path){
        
        $found = false;
        foreach($this->extensionFilter as $extension) {
            
            if(empty($extension)){
                return true;
            }
            
            $extension = trim(strtolower(str_replace(".", "", $extension)));
            $fileExt = strtolower(pathinfo($path, PATHINFO_EXTENSION));
            
            if($extension == $fileExt) {
                $found = true;
            }
            
        }
        
        return $found;
        
    }
}